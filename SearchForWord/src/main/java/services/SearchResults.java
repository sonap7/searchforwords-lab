package services;

import java.util.List;

public class SearchResults {
    public List<String> text;
    public boolean found;
    private List<Integer> page;

    public SearchResults() {
    }

    public SearchResults(List<String> text) {
        this.text = text;
    }

    public void setText(List<String> text) {
        this.text = text;
    }

    public boolean isFound() {
        return found;
    }

    public void setFound(boolean found) {
        this.found = found;
    }

    public List<Integer> getPage() {
        return page;
    }


    @Override
    public String toString() {
        return "SearchResults{" +
                "text='" + text + '\'' +
                ", found=" + found +
                ", page=" + page +
                '}';
    }
}
