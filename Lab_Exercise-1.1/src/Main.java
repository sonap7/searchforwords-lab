import services.InitWords;
import services.PDFTextFinder;
//import services.SearchResults;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException, IndexOutOfBoundsException, FileNotFoundException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Scanner in = new Scanner(System.in);
        int choise;

        System.out.println("Please enter the path of the file: ");
        String pathname = br.readLine();
        System.out.println("Please enter a word for search: ");
        String word = br.readLine();

        List<String> wordlist = new ArrayList<>();
        wordlist.add(word);

//        do{
//        System.out.println("Do you want to enter another word for search?");
//        System.out.println("Press\n\r1. for Yes\n\ror\n\r2. for No");
//        choise = in.nextInt();
//        if (choise == 1){
//            System.out.println("Please enter a word for search: ");
//            word = br.readLine();
//            wordlist.add(word);
//        }
//        else{
//            break;
//        }
//        }while (choise == 1);

        PDFTextFinder pdfTextFinder= new PDFTextFinder();
        pdfTextFinder.findTextFromPDFDocument(pathname, wordlist);
    }
}
