package services;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WordTextFinder extends SearchResults {

    public void readTextFromDocDocument (String pathname, List<String> wordList) throws IOException{
        File file = null;
        List<Integer> loc = new ArrayList<>();
        List<String> dwords = new ArrayList<>();
        try {
            file = new File(pathname);
            FileInputStream fis = new FileInputStream(file.getAbsolutePath());
            XWPFDocument document = new XWPFDocument(fis);
            List<XWPFParagraph> paragraphs = document.getParagraphs();

            for (String text : wordList){
                dwords.add(text);
            }

            for (int k = 0; k <= wordList.size(); k++) {
                for( int i=0; i<paragraphs.size(); i++){
                    String para = paragraphs.get(i).getParagraphText();
                    if (para.equals(wordList.get(k))) {
                        loc.add(i+1);
                    }
                }
            }
        }
        catch(Exception exep){}

        for (String dword : dwords){
            if (!(loc.size()==0)){
                setFound(true);
                System.out.println("The word: "+dword+" ,Found: "+found+" ,at paragraph: "+loc);
            }
            else{
                setFound(false);
                System.out.println("The word: "+dword+" ,Found: "+found);
            }
        }
    }
}
