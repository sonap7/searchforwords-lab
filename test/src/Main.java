import services.InitWords;
import services.WordTextFinder;
import services.SearchResults;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Please enter the path of the file: ");
        String pathname = br.readLine();
        System.out.println("Please enter a word for search: ");
        String word = br.readLine();

        List<String> wordlist = new ArrayList<>();
        wordlist.add(word);

        WordTextFinder wordTextFinder = new WordTextFinder();
        wordTextFinder.readTextFromDocDocument(pathname, wordlist);
    }
}